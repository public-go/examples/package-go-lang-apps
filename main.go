package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	lollypop := "🍭"
	sailboat := "⛵"
	fmt.Println("This is my app!!!", lollypop, sailboat)

	resp, err := http.Get("https://google.nl/")
	if err != nil {
		log.Fatalf("Unable to reach server: %v", err)
	}
	log.Println("Server could be reached and gives status: ", resp.Status)
}
