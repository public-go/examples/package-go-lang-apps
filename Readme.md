# Example on how to package go app

run an app: `go run main.go`

build an binary on your own system: `go build main.go`

build a binary for windows: `GOOS=windows GOARCH=amd64 go build main.go`

build a binary for mac: `GOOS=darwin GOARCH=amd64 go build main.go`

build a binary for linux: `GOOS=linux GOARCH=amd64 go build main.go`

since I use this for training purposes, the docker commands will fail out of the box.

build a docker image with a [Dockerfile](./Dockerfile): `docker build -t test .`

run the docker image: `docker run -it --rm test`