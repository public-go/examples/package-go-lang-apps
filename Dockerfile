FROM golang as builder
ADD main.go /go/
# RUN CGO_ENABLED=0 go build main.go
RUN go build main.go
RUN ls -alh

FROM scratch
COPY --from=builder /go/main /
CMD ["/main"]
